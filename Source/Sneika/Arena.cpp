// Fill out your copyright notice in the Description page of Project Settings.


#include "Arena.h"
#include "Wall.h"

AArena::AArena()
{
	WallHight = 2;
	WallWidth = 0.5;
	ArenaWidth = 5;
	ArenaHigh = 5;
}

void AArena::BeginPlay()
{
	Super::BeginPlay();
	SetArenaScale();
	FVector Origin;
	FVector Extend;
	GetActorBounds(false, Origin, Extend);

	FVector LeftWallLoc = Origin - FVector(Extend.X, 0, 0);
	FVector VerticalWallScale = FVector(WallWidth, Extend.Y / 50 + WallWidth, WallHight);
	SpawnWall(LeftWallLoc, VerticalWallScale);

	FVector RighttWallLoc = Origin + FVector(Extend.X, 0, 0);
	SpawnWall(RighttWallLoc, VerticalWallScale);

	FVector UpWallLoc = Origin + FVector(0, Extend.Y, 0);
	FVector HorizontalWallScale = FVector(Extend.X / 50 + WallWidth, WallWidth, WallHight);
	SpawnWall(UpWallLoc, HorizontalWallScale);

	FVector DownWallLoc = Origin - FVector(0, Extend.Y, 0);
	SpawnWall(DownWallLoc, HorizontalWallScale);
}

void AArena::SetArenaScale()
{
	SetActorScale3D(FVector(ArenaWidth, ArenaHigh, 0.1));
	
}

void AArena::GetRandFoodSpawnLoc(FVector& OutLoc)
{
	FVector Origin;
	FVector Extend;
	GetActorBounds(false, Origin, Extend);
	float SpawnZ = 20;
	float SpawnX = FMath::FRandRange(Origin.X - Extend.X + WallWidth * 100, Origin.X + Extend.X - WallWidth * 100);
	float SpawnY = FMath::FRandRange(Origin.Y - Extend.Y + WallWidth * 100, Origin.Y + Extend.Y - WallWidth * 100);
	OutLoc = FVector(SpawnX, SpawnY, SpawnZ);
}

void AArena::SpawnWall(FVector SpawnLoc, FVector SpawnScale)
{
	FTransform SpawnTransform = FTransform(FRotator(), SpawnLoc, SpawnScale);
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	GetWorld()->SpawnActor<AWall>(WallClass, SpawnTransform, SpawnParams);
}
