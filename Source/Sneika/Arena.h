// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Arena.generated.h"
class AWall;

/**
 * 
 */
UCLASS()
class SNEIKA_API AArena : public AStaticMeshActor
{
	GENERATED_BODY()
public:
	AArena();
	
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AWall> WallClass;

	UPROPERTY(EditDefaultsOnly)
		float ArenaWidth;
	UPROPERTY(EditDefaultsOnly)
		float ArenaHigh;
	virtual void BeginPlay() override;
	void SetArenaScale();
	void GetRandFoodSpawnLoc(FVector& OutLoc);
private:
	void SpawnWall(FVector SpawnLoc, FVector SpawnScale);
	float WallHight;
	float WallWidth;
	
};
