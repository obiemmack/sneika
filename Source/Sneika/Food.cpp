// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement(1);
			Snake->SpawnFood();
			int RandInt = UKismetMathLibrary::RandomIntegerInRange(1, 6);

			switch (RandInt)
			{
			case 1:
				Snake->SpawnFast();
				break;
			case 2:
				Snake->SpawnSlow();
				break;
			case 3:
				Snake->SpawnWall();

			default:
				break;
			}

		}
	}
	Destroy();
}

