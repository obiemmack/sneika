// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElement.h"
#include "Interactable.h"
#include "Arena.h"
#include "Food.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementsSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	DefaultSpeed = MovementSpeed;
	SpawnArena();
	AddSnakeElement(5);
	SpawnFood();
	
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	BonusTimer();
	

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementsSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElement* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if (ElemIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
		
	}
	
	
	
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);


	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementsSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementsSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementsSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementsSize;
		break;
	default:
		break;
	}
	SnakeElements[0]->ToggleCollision();
	
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		SnakeElements[0]->SetActorHiddenInGame(false);
		SnakeElements[i]->SetActorHiddenInGame(false);
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	

}

void ASnakeBase::SnakeElementOverlap(ASnakeElement* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}

	}
}

void ASnakeBase::SpawnArena()
{
	FActorSpawnParameters SpawnParam;
	SpawnParam.Owner = this;
	SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	FTransform SpawnTransform = FTransform(FRotator(), FVector(0, 0, 0), FVector(0, 0, 0));
	AArena* NewArena = GetWorld()->SpawnActor<AArena>(ArenaClass, FVector(0, 0, 0), FRotator(), SpawnParam);
	Arena = NewArena;
}

void ASnakeBase::SpawnFood()
{
	FVector SpawnLocation;
	Arena->GetRandFoodSpawnLoc(SpawnLocation);
	GetWorld()->SpawnActor<AFood>(FoodClass, SpawnLocation, FRotator(), FActorSpawnParameters());
}

void ASnakeBase::BonusTimer()
{
	if (Timer != 0)
	{
		Timer--;
		UE_LOG(LogTemp, Warning, TEXT("Bonus is: %i"), Timer)
	}
	else
	{
		MovementSpeed = DefaultSpeed;
		SetActorTickInterval(MovementSpeed);
	}
}

void ASnakeBase::FastBonus()
{
	if (MovementSpeed == DefaultSpeed)
	{
		MovementSpeed = MovementSpeed - 0.25;
		SetActorTickInterval(MovementSpeed);
		Timer = 20;
		UE_LOG(LogTemp, Warning, TEXT("Bonus is: %f"), MovementSpeed)
	}
	else
	{
		Timer = 20;
		UE_LOG(LogTemp, Warning, TEXT("Bonus is: %f"), MovementSpeed)
	}
}

void ASnakeBase::SlowBonus()
{
	if (MovementSpeed == DefaultSpeed)
	{
		MovementSpeed = MovementSpeed + 0.25;
		SetActorTickInterval(MovementSpeed);
		Timer = 10;
		UE_LOG(LogTemp, Warning, TEXT("Bonus is: %f"), MovementSpeed)
	}
	else
	{
		Timer = 10;
		UE_LOG(LogTemp, Warning, TEXT("Bonus is: %f"), MovementSpeed)
	}
}

void ASnakeBase::SpawnFast()
{
	FVector SpawnLocation;
	Arena->GetRandFoodSpawnLoc(SpawnLocation);
	GetWorld()->SpawnActor<AFastBonus>(FastBonusClass, SpawnLocation, FRotator(), FActorSpawnParameters());
}

void ASnakeBase::SpawnSlow()
{
	FVector SpawnLocation;
	Arena->GetRandFoodSpawnLoc(SpawnLocation);
	GetWorld()->SpawnActor<ASlowBonus>(SlowBonusClass, SpawnLocation, FRotator(), FActorSpawnParameters());
}

void ASnakeBase::SpawnWall()
{
	FVector SpawnLocation;
	Arena->GetRandFoodSpawnLoc(SpawnLocation);
	GetWorld()->SpawnActor<AWall>(WallClass, SpawnLocation, FRotator(), FActorSpawnParameters());
}

