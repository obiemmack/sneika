// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Arena.h"
#include "Food.h"
#include "FastBonus.h"
#include "SlowBonus.h"
#include "Wall.h"
#include "SnakeBase.generated.h"

class ASnakeElement;
class AFood;
class AFastBonus;
class ASlowBonus;
class AWall;


UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNEIKA_API ASnakeBase : public AActor
{
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
	GENERATED_BODY()
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElement> SnakeElementClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AArena> ArenaClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFastBonus> FastBonusClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASlowBonus> SlowBonusClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AWall> WallClass;
	class AArena* Arena;
	UPROPERTY(EditDefaultsOnly)
		float ElementsSize;
	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;
	float DefaultSpeed;
	UPROPERTY()
		TArray <ASnakeElement*> SnakeElements;
	UPROPERTY()
		EMovementDirection LastMoveDirection;
	int Timer = 0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void AddSnakeElement(int ElementsNum = 1);
	void Move();
	void SnakeElementOverlap(ASnakeElement* OverlappedElement, AActor* Other);
	void SpawnArena();
	void SpawnFood();
	void BonusTimer();
	void FastBonus();
	void SlowBonus();
	void SpawnFast();
	void SpawnSlow();
	void SpawnWall();
};
