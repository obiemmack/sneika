// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SneikaGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNEIKA_API ASneikaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
