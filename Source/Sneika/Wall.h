// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Interactable.h"
#include "Wall.generated.h"

/**
 * 
 */
UCLASS()
class SNEIKA_API AWall : public AStaticMeshActor, public IInteractable
{
	GENERATED_BODY()
		virtual void Interact(AActor* Interactor, bool bIsHead);
	
};
